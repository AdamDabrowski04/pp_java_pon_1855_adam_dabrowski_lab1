package pwr.lab1.zad2;

public class WordHasherBuilder
{
    private int offset_=0;
    public WordHasherBuilder() {}
    public WordHasherBuilder offset(int offset_)
    {
        this.offset_=offset_;
        return this;
    }
    public WordHasher buildWordHasher()
    {
        return new WordHasher(offset_);
    }

}
