package pwr.lab1.zad1;

/**
 * Created by Adam on 2015-05-04.
 */
public class Fibonacci {
   public long sum;

   public long sumEvenFibIterationUpToNum(long n) {
      long x = 0, y = 1, z = 1;
      sum = 0;
      while (x < n) {
         x = y;
         y = z;
         z = x + y;
         sum += ((x % 2 == 0) ? x : 0);
      }

      return sum;
   }

   @Override
   public String toString() {
      return Long.toString(sum);

   }
}