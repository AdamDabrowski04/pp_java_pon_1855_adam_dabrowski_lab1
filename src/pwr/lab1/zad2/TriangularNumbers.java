package pwr.lab1.zad2;
import java.util.Iterator;
import java.util.Vector;

/**
 * Created by Adam on 2015-05-04.
 */
public class TriangularNumbers {
    private Vector<Integer> triangularNumbers;
    private Integer  maxTriangularIndex;

    public TriangularNumbers() {
        triangularNumbers = new Vector();
        maxTriangularIndex = 500;
        for (int i = 1; i <= maxTriangularIndex; i++) {
            triangularNumbers.add((i * (i + 1)) / 2);
        }

    }
        public boolean isTriangular(Integer num){
            while(num> triangularNumbers.lastElement()){
                this.calculateMoreTriangularNumbers();
            }
            for(Integer aa : triangularNumbers) {
                if (aa == num) {
                        return true;
                    }
                }
            return false;
        }


    private void calculateMoreTriangularNumbers()
    {
        for (int i = maxTriangularIndex; i <= maxTriangularIndex*2; i++) {
            triangularNumbers.add((i * (i + 1)) / 2);
        }
        maxTriangularIndex=maxTriangularIndex*2;
    }


}
