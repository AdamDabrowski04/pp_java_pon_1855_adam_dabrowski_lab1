package pwr.lab1.zad2;
//import pwr.lab1.zad2.WordHasher;
//import pwr.lab1.zad2.WordHasherBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Adam on 2015-05-22.
 */
public class WordHasherTest {
    WordHasher wordHasher1;
    String alphabetUC="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    String alphabetLC="abcdefghijklmnopqrstuvwxyz";
    @Before
    public void setUp() throws Exception {
     wordHasher1=new WordHasherBuilder().offset(0).buildWordHasher();
        System.out.println("@Before - setUp");
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void SumOfLettersInAlphabetLowerCase()
    {

        int sum=0;
        for(int i=1;i<=26;i++)
        {
            sum+=i;
        }
        assertEquals(sum,wordHasher1.sumOfLetterNumericValues(alphabetLC));
        System.out.println("@Test - test sum of letters in alphabet lower case");
    }
    @Test
    public void SumOfLettersInAlphabetUpperCase()
    {
        int sum=0;
        for(int i=1;i<=26;i++)
        {
            sum+=i;
        }
        assertEquals(sum,wordHasher1.sumOfLetterNumericValues(alphabetUC));
        System.out.println("@Test - test sum of letters in alphabet UPPER case");
    }

    @Test
    public void SumOfLettersInAlphabetUpperCaseEqualsLowerCase(){
        int sum1=0, sum2=0;

        assertEquals(wordHasher1.sumOfLetterNumericValues(alphabetLC),wordHasher1.sumOfLetterNumericValues(alphabetUC));
        System.out.println("@Test - test Lower Upper case insensitivity");
    }

}