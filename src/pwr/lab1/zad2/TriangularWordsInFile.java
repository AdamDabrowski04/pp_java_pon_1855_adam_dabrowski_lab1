package pwr.lab1.zad2;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
/**
 * Created by Adam on 2015-05-06.
 */
public class TriangularWordsInFile {

    String sourceFile;
    WordHasher wordHasher= new WordHasherBuilder().offset(0).buildWordHasher();
    TriangularNumbers triangularNumbers= new TriangularNumbers();
    List<String> wordList;
    int triangularWordsCounter=0;
    public TriangularWordsInFile(String sourceFile)
    {
        this.sourceFile=sourceFile;
    }
    public int checkIt()
    {
        FileReader fileReader= new FileReader(sourceFile);
        wordList=fileReader.readAndParse();
        for(String word : wordList)
        {
            int tmp=wordHasher.sumOfLetterNumericValues(word);
            if(triangularNumbers.isTriangular(tmp))
            {
                triangularWordsCounter++;
                //System.out.println(word);
            }
        }
        return triangularWordsCounter;
    }

    @Override
    public String toString(){
        return Integer.toString(triangularWordsCounter);
    }

}
