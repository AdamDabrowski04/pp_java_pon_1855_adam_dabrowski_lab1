package pwr.lab1.zad2;

/**
 * Created by Adam on 2015-05-04.
 */
public class WordHasher {

    int offset;
    WordHasher(int offset){
        this.offset=offset;
    }
    public int sumOfLetterNumericValues(String word)
    {
        int Sum =0;
        for(int i =0; i<word.length();i++){
            Sum +=Character.getNumericValue(word.charAt(i))-9 -offset;
        }
        return Sum;
    }
}

