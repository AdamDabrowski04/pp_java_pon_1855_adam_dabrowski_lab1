package pwr.lab1.zad2;
import java.io.File;
import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Adam on 2015-05-04.
 */
public class FileReader {
    String filePath ;

    FileReader(String filePath)
    {
        this.filePath=filePath;
    }

    public List<String> readAndParse(){
        File file =new File(filePath);
        String word;
        List<String>  wordList =new LinkedList();
        try(Scanner sc= new Scanner(file)) {
            sc.useDelimiter(",");
            while(sc.hasNext())
            {
                word=sc.next();
                word=word.substring(1, word.length() - 1);
                wordList.add(word);
            }
        }
        catch(FileNotFoundException e)
        {
            System.out.println("File not found "+ e);
        }
        return wordList;
    }

}
